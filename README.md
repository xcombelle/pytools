### pytools ###

A set of small, unobtrusive and cross platform developer utilities. Tested under Linux, OSX and Windows.

```
notif.py - Gmail notifier supporting multiple accounts
timelog.py - Activity time tracker and reminder
```

### Requirements ###

```
Python-3
PyQt-5
PyYAML
```

### Bugs ###

* Tray icon occasionally disappears in OSX (https://bugreports.qt.io/browse/QTBUG-42910)

### Screenshots ###

![notif-shot.png](https://bitbucket.org/qx89l4/pytools/raw/master/misc/notif-shot.png)

![timelog-shot-mac.png](https://bitbucket.org/qx89l4/pytools/raw/master/misc/timelog-shot-mac.png)

![timelog-shot-win.png](https://bitbucket.org/qx89l4/pytools/raw/master/misc/timelog-shot-win.png)
