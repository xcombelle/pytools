#!/usr/bin/env python3
#
# Copyright (c) 2015 Łukasz Strzygowski
# Distributed under the terms of GPL-2 License.
#

"""
Simple time logger
"""

import collections
import copy
import json
import os
import sys
import uuid

from PyQt5 import QtCore, QtGui, QtWidgets

import common

class Application(metaclass = common.Singleton):
    """Global application manager"""

    # initialization

    def __init__(self):
        """Initialize"""

        self._storage = None
        self._controller = None
        self._current_activity = None

    # public properties

    @property
    def root(self):
        """Return application root directory"""

        return os.path.dirname(sys.argv[0])

    @property
    def current_activity(self):
        """Return current activity or None"""

        return self._current_activity

    # public methods

    def add_reminder(self, reminder):
        """Add a reminder to the storage"""

        return self._storage.add_reminder(reminder)

    def pop_expired_reminders(self):
        """Fetch and remove all expired reminders from the storage"""

        rems = self._storage.pop_expired_reminders()

        # ignore reminders attached to finished activities
        activity = self.current_activity
        key = activity['key'] if activity else ''
        rems = [x for x in rems if x['activity_key'] in ('', key)]

        return rems

    def add_activity(self, activity):
        """Add an activity to the storage"""

        return self._storage.add_activity(activity)

    def start_activity(self, activity):
        """Start a new activity"""

        self.finish_activity()
        self._current_activity = activity

    def finish_activity(self):
        """Finish and save current activity"""

        activity = self.current_activity

        if not activity:
            return

        now = QtCore.QDateTime.currentMSecsSinceEpoch()
        activity['duration'] = now - activity['start']
        self.add_activity(activity)

        self._current_activity = None

    def recent_activities(self):
        """Fetch recent activities from the storage"""

        return self._storage.recent_activities(100)

    def project_suggestions(self):
        """Fetch recent activity project names"""

        projects = (x['project'] for x in self.recent_activities())
        projects = collections.OrderedDict.fromkeys(projects).keys()

        return projects

    def desc_suggestions(self):
        """Fetch recent activity descriptions"""

        descs = (x['desc'] for x in self.recent_activities())
        descs = collections.OrderedDict.fromkeys(descs).keys()

        return descs

    def position_window(self, window):
        """Position dialog window"""

        self._controller.position_window(window)

    def run(self):
        """Execute application"""

        self._storage = Storage()
        self._controller = AppController()

        return self._controller.run()

# convenience shortcut for getting Application singleton
app = Application

class Storage:
    """File storage manager"""

    # initialization

    def __init__(self):
        """Initialize object and load data"""

        self._path = os.path.join(app().root, 'data', 'timelog.json')
        self._data = None
        self._load()

    # private methods

    def _load(self):
        """Load data from file, or initialize empty"""

        default = {
            'activities': [],
            'reminders': [],
        }

        if not os.path.exists(self._path):
            self._data = default
            return

        with open(self._path) as fp:
            self._data = json.load(fp)

    def _save(self):
        """Save data to file"""

        with open(self._path, 'w') as fp:
            json.dump(self._data, fp, indent = 4)

    # public methods

    def add_activity(self, activity):
        """Append new activity and save"""

        self._data['activities'].append(activity)
        self._data['activities'].sort(key = lambda x: -x['start'])
        self._save()

    def recent_activities(self, count):
        """Fetch a givent amount of recent activities, ordered from the most recent"""

        activities = self._data['activities'][:count]

        return activities

    def add_reminder(self, reminder):
        """Append new reminder and save"""

        self._data['reminders'].append(reminder)
        self._data['reminders'].sort(key = lambda x: x['time'])

        self._save()

    def pop_expired_reminders(self):
        """Remove and return expired reminders"""

        now = QtCore.QDateTime.currentMSecsSinceEpoch()

        all_rems = self._data['reminders']
        exp_rems = [x for x in all_rems if x['time'] <= now]
        self._data['reminders'] = [x for x in all_rems if not x in exp_rems]

        if exp_rems:
            self._save()

        return exp_rems

class AppController:
    """Application interface controller"""

    # initialization

    def __init__(self):
        """Initialize object, without invoking GUI"""

        self._qapp = None
        self._timer = None

        self._menu = None
        self._tray_icon = None

        self._active_icon = None
        self._inactive_icon = None

        self._start_action = None
        self._finish_action = None
        self._timer_action = None

        self._recent_activities_controller = None

    # private methods

    def _setup_menu(self):
        """Setup menu for the tray icon"""

        self._menu = menu = QtWidgets.QMenu()

        self._start_action = menu.addAction('Start activity', self._start_activity_clicked)
        self._finish_action = menu.addAction('Finish activity', self._finish_activity_clicked)
        menu.addSeparator()

        menu.addAction('Add reminder', self._add_reminder_clicked)
        menu.addAction('Recent activities', self._recent_activities_clicked)
        menu.addSeparator()

        self._timer_action = menu.addAction('Status')
        self._timer_action.setEnabled(False)
        menu.addSeparator()

        menu.addAction('Exit', self._exit_clicked)

    def _setup_icon(self):
        """Setup tray icon"""

        icopath = os.path.join(app().root, 'icons')
        self._active_icon = QtGui.QIcon(os.path.join(icopath, 'timelog-tray-active'))
        self._inactive_icon = QtGui.QIcon(os.path.join(icopath, 'timelog-tray-inactive'))

        self._tray_icon = QtWidgets.QSystemTrayIcon()
        self._tray_icon.setIcon(self._inactive_icon)
        self._tray_icon.setContextMenu(self._menu)
        self._tray_icon.activated.connect(self._icon_activated)

    def _update_timer(self):
        """Update timer action"""

        activity = app().current_activity

        if not activity:
            text = "Status: Idle"
        else:
            now = QtCore.QDateTime.currentMSecsSinceEpoch()
            msecs = now - activity['start']
            text = "Working: %s" % common.format_duration(msecs)

        self._timer_action.setText(text)

    def _update(self):
        """Update the entire UI"""

        activity = app().current_activity
        is_working = not activity is None

        # update icon
        icon = self._active_icon if is_working else self._inactive_icon
        self._tray_icon.setIcon(icon)

        # update action text
        title = 'Finish activity' if is_working else 'Start activity'
        self._start_action.setVisible(not is_working)
        self._finish_action.setVisible(is_working)

        # update timer
        self._update_timer()

        # update recent activity window
        if self._recent_activities_controller:
            self._recent_activities_controller.reload_data()

    # event handlers

    def _timer_timed_out(self):
        """Handle timer tick"""

        self._update_timer()

        # fetch and remove from storage all the expired reminders
        reminders = app().pop_expired_reminders()
        if not reminders:
            return

        # display reminder box and optionally finish current activity
        do_finish = ReminderController.run(reminders)
        if do_finish:
            app().finish_activity()
            self._update()

    def _start_activity_clicked(self):
        """Handle start-activity button"""

        activity, reminder = NewActivityController.run()

        # set a new reminder
        if reminder:
            app().add_reminder(reminder)

        # append a completed activity
        if activity and activity['duration'] > 0:
            app().add_activity(activity)

        # start an ongoing activity
        if activity and activity['duration'] == 0:
            app().start_activity(activity)

        self._update()

    def _finish_activity_clicked(self):
        """Handle finish-activity button"""

        app().finish_activity()
        self._update()

    def _recent_activities_clicked(self):
        """Handle recent-activities button"""

        if not self._recent_activities_controller:
            self._recent_activities_controller = RecentActivitiesController()
        self._recent_activities_controller.show()

    def _add_reminder_clicked(self):
        """Handle add-reminder button"""

        reminder = NewReminderController.run()
        if reminder:
            app().add_reminder(reminder)

    def _exit_clicked(self):
        """Handle exit button"""

        app().finish_activity()
        self._qapp.quit()

    def _icon_activated(self, reason):
        """Handle tray icon activation"""

        # display menu on a single left click, except for OSX, which handles it itself
        if reason == QtWidgets.QSystemTrayIcon.Trigger and sys.platform != 'darwin':
            pos = QtGui.QCursor.pos()
            self._menu.popup(pos)

    # public methods

    def position_window(self, window):
        """Positin window near the tray icon"""

        ico_geo = self._tray_icon.geometry()
        win_geo = window.geometry()
        scr_geo = QtWidgets.QApplication.primaryScreen().virtualGeometry()
        margin = 30

        win_geo.moveLeft(ico_geo.left() + ico_geo.width() / 2 - win_geo.width() / 2)
        win_geo.moveTop(ico_geo.bottom() + margin)

        if win_geo.left() < scr_geo.left():
            win_geo.moveLeft(margin)

        if win_geo.right() > scr_geo.right():
            win_geo.moveRight(scr_geo.right() - margin)

        if win_geo.top() > scr_geo.bottom():
            win_geo.moveBottom(ico_geo.top() - margin)

        window.setGeometry(win_geo)

    def run(self):
        """Execute application"""

        # create Qt application
        self._qapp = QtWidgets.QApplication(sys.argv)
        self._qapp.setQuitOnLastWindowClosed(False)

        # setup user interface
        self._setup_menu()
        self._setup_icon()
        self._update()
        self._tray_icon.show()

        # setup scheduled timer
        self._timer = QtCore.QTimer()
        self._timer.setInterval(1000)
        self._timer.timeout.connect(self._timer_timed_out)
        self._timer.start()

        # start event loop
        return self._qapp.exec_()

class ReminderController:
    """Reminder dialog controller"""

    # public methods

    @classmethod
    def run(cls, reminders):
        """Show reminders. Returns True if user decides to finish current activity"""

        # window flags
        flags = QtCore.Qt.CustomizeWindowHint | QtCore.Qt.WindowTitleHint | \
                QtCore.Qt.WindowCloseButtonHint | QtCore.Qt.WindowStaysOnTopHint | \
                QtCore.Qt.Dialog

        # message box
        box = QtWidgets.QMessageBox()
        box.resize(500, 200)
        box.setWindowTitle('Reminder')
        box.setWindowFlags(flags)
        box.setText('Timelog Reminder')
        box.setInformativeText("\n\n".join(x['text'] for x in reminders) + "\n")
        box.addButton('Dismiss', QtWidgets.QMessageBox.RejectRole)
        if app().current_activity:
            box.addButton('Finish activity', QtWidgets.QMessageBox.AcceptRole)

        # FIXME: find a better way to set a minimum width
        spacer = QtWidgets.QSpacerItem(200, 0, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        box.layout().addItem(spacer, box.layout().rowCount(), 0, 1, box.layout().columnCount())

        # show message
        # FIXME: find a better way to raise dialog
        QtCore.QTimer.singleShot(300, lambda: box.raise_())
        ret = box.exec_()
        do_finish = ret == QtWidgets.QDialog.Accepted

        return do_finish

class NewReminderController:
    """New-reminder dialog controller"""

    # initialization

    def __init__(self):
        """Initialize object"""

        self._dialog = None
        self._text_edit = None
        self._time_combo = None
        self._ok_button = None

    # private methods

    def _is_valid(self):
        """Check if dialog is correctly filled to be accepted"""

        valid = True

        valid = valid and len(self._text_edit.text()) > 0
        valid = valid and self._time_combo.currentText().isdigit()

        return valid

    def _update(self):
        """Update widgets after user input"""

        valid = self._is_valid()
        self._ok_button.setEnabled(valid)

    def _setup(self):
        """Setup dialog and widgets"""

        # text edit
        self._text_edit = QtWidgets.QLineEdit()
        self._text_edit.setText('TimeLog Reminder')
        self._text_edit.textChanged.connect(lambda x: self._update())

        # time labels and combo
        time_label = QtWidgets.QLabel('Show after:')
        self._time_combo = QtWidgets.QComboBox()
        self._time_combo.setEditable(True)
        self._time_combo.lineEdit().setAlignment(QtCore.Qt.AlignRight)
        for x in (5, 15, 30, 45, 60, 90):
            self._time_combo.addItem("%d" % x, x)
        self._time_combo.editTextChanged.connect(lambda x: self._update())
        time_label_min = QtWidgets.QLabel('minutes')
        time_layout = QtWidgets.QHBoxLayout()
        time_layout.addWidget(time_label, 0)
        time_layout.addWidget(self._time_combo, 1)
        time_layout.addWidget(time_label_min, 0)

        # buttons
        cancel_button = QtWidgets.QPushButton('Cancel')
        cancel_button.clicked.connect(lambda: self._dialog.reject())
        self._ok_button = QtWidgets.QPushButton('Add')
        self._ok_button.clicked.connect(lambda: self._dialog.accept())
        self._ok_button.setDefault(True)
        button_layout = QtWidgets.QHBoxLayout()
        button_layout.addStretch(1)
        button_layout.addWidget(cancel_button)
        button_layout.addWidget(self._ok_button)

        # dialog layout
        main_layout = QtWidgets.QVBoxLayout()
        main_layout.setSpacing(5)
        main_layout.addWidget(self._text_edit)
        main_layout.addSpacing(5)
        main_layout.addLayout(time_layout)
        main_layout.addSpacing(10)
        main_layout.addLayout(button_layout)

        # window flags
        flags = QtCore.Qt.CustomizeWindowHint | QtCore.Qt.WindowTitleHint | \
                QtCore.Qt.WindowSystemMenuHint | QtCore.Qt.WindowCloseButtonHint | \
                QtCore.Qt.WindowStaysOnTopHint | QtCore.Qt.Dialog

        # dialog
        self._dialog = QtWidgets.QDialog()
        self._dialog.setWindowTitle('Add reminder')
        self._dialog.setWindowFlags(flags)
        self._dialog.setLayout(main_layout)
        self._dialog.resize(300, 0)
        self._dialog.layout().update()
        self._dialog.layout().activate()
        app().position_window(self._dialog)

    def _reminder(self):
        """Return configured reminder"""

        text = self._text_edit.text()
        now = QtCore.QDateTime.currentMSecsSinceEpoch()
        reminder_mins = int(self._time_combo.currentText())
        time = now + reminder_mins * 1000 * 60

        return {
            'text': text,
            'time': time,
            'activity_key': '',
        }

    # public methods

    @classmethod
    def run(cls):
        """Show dialog and return configured reminder, or None"""

        cont = cls()
        cont._setup()

        # FIXME: find a better way to raise dialog
        QtCore.QTimer.singleShot(300, lambda: cont._dialog.raise_())

        ret = cont._dialog.exec_()
        reminder = cont._reminder() if ret == QtWidgets.QDialog.Accepted else None

        return reminder

class NewActivityController:
    """New-activity dialog controller"""

    # initialization

    def __init__(self):
        """Initialize object"""

        self._dialog = None
        self._desc_combo = None
        self._project_combo = None
        self._reminder_check = None
        self._reminder_combo = None
        self._start_time_check = None
        self._start_time_edit = None
        self._duration_check = None
        self._duration_edit = None
        self._ok_button = None

    # private methods

    def _is_valid(self):
        """Check if dialog is properly filled to be accepted"""

        valid = True

        valid = valid and len(self._desc_combo.currentText()) > 0
        valid = valid and len(self._project_combo.currentText()) > 0

        return valid

    def _update(self):
        """Update form elements after user input"""

        valid = self._is_valid()
        self._ok_button.setEnabled(valid)

        self._reminder_combo.setEnabled(self._reminder_check.isChecked())
        self._start_time_edit.setEnabled(self._start_time_check.isChecked())
        self._duration_edit.setEnabled(self._duration_check.isChecked())

    def _setup(self):
        """Setup widgets"""

        # desc label and combo box
        desc_label = QtWidgets.QLabel('Activity:')
        self._desc_combo = QtWidgets.QComboBox()
        self._desc_combo.setEditable(True)
        for x in app().desc_suggestions():
            self._desc_combo.addItem(x)
        self._desc_combo.editTextChanged.connect(lambda x: self._update())
        desc_layout = QtWidgets.QVBoxLayout()
        desc_layout.setSpacing(1)
        desc_layout.addWidget(desc_label)
        desc_layout.addWidget(self._desc_combo)

        # project label and combo box
        project_label = QtWidgets.QLabel('Project:')
        self._project_combo = QtWidgets.QComboBox()
        self._project_combo.setEditable(True)
        for x in app().project_suggestions():
            self._project_combo.addItem(x)
        self._project_combo.editTextChanged.connect(lambda x: self._update())
        project_layout = QtWidgets.QVBoxLayout()
        project_layout.setSpacing(1)
        project_layout.addWidget(project_label)
        project_layout.addWidget(self._project_combo)

        # reminder check box and combo box
        self._reminder_check = QtWidgets.QCheckBox('Add reminder')
        self._reminder_check.setChecked(True)
        self._reminder_check.clicked.connect(lambda x: self._update())
        self._reminder_combo = QtWidgets.QComboBox()
        for x in (5, 15, 30, 45, 60, 90):
            self._reminder_combo.addItem("%d min" % x, x)
        self._reminder_combo.setCurrentText("45 min")
        reminder_layout = QtWidgets.QHBoxLayout()
        reminder_layout.addWidget(self._reminder_check, 1)
        reminder_layout.addWidget(self._reminder_combo, 0)

        # start time check box and datetime edit
        self._start_time_check = QtWidgets.QCheckBox('Start time')
        self._start_time_check.clicked.connect(lambda x: self._update())
        self._start_time_edit = QtWidgets.QDateTimeEdit()
        self._start_time_edit.setDateTime(QtCore.QDateTime.currentDateTime())
        start_time_layout = QtWidgets.QHBoxLayout()
        start_time_layout.addWidget(self._start_time_check, 1)
        start_time_layout.addSpacing(50)
        start_time_layout.addWidget(self._start_time_edit, 0)

        # duration check box and time edit
        self._duration_check = QtWidgets.QCheckBox('Duration')
        self._duration_check.clicked.connect(lambda x: self._update())
        self._duration_edit = QtWidgets.QTimeEdit()
        self._duration_edit.setDisplayFormat('HH:mm')
        duration_layout = QtWidgets.QHBoxLayout()
        duration_layout.addWidget(self._duration_check, 1)
        duration_layout.addWidget(self._duration_edit, 0)

        # cancel and ok buttons
        cancel_button = QtWidgets.QPushButton('Cancel')
        cancel_button.clicked.connect(lambda checked: self._dialog.reject())
        self._ok_button = QtWidgets.QPushButton('OK')
        self._ok_button.clicked.connect(lambda checked: self._dialog.accept())
        self._ok_button.setDefault(True)
        button_layout = QtWidgets.QHBoxLayout()
        button_layout.addStretch(1)
        button_layout.addWidget(cancel_button)
        button_layout.addWidget(self._ok_button)

        # main layout
        main_layout = QtWidgets.QVBoxLayout()
        main_layout.setSpacing(5)
        main_layout.addLayout(desc_layout)
        main_layout.addLayout(project_layout)
        main_layout.addSpacing(15)
        main_layout.addStretch(1)
        main_layout.addLayout(reminder_layout)
        main_layout.addLayout(start_time_layout)
        main_layout.addLayout(duration_layout)
        main_layout.addSpacing(15)
        main_layout.addStretch(1)
        main_layout.addLayout(button_layout)

        # window flags
        flags = QtCore.Qt.CustomizeWindowHint | QtCore.Qt.WindowTitleHint | \
                QtCore.Qt.WindowCloseButtonHint | QtCore.Qt.WindowStaysOnTopHint | \
                QtCore.Qt.Dialog

        # dialog
        self._dialog = QtWidgets.QDialog()
        self._dialog.setWindowTitle('New activity')
        self._dialog.setWindowFlags(flags)
        self._dialog.setLayout(main_layout)
        self._dialog.resize(0, 0)
        self._dialog.layout().update()
        self._dialog.layout().activate()
        app().position_window(self._dialog)

        self._update()

    def _activity(self):
        """Return configured activity"""

        if not self._is_valid():
            return None

        desc = self._desc_combo.currentText()
        project = self._project_combo.currentText()
        start = QtCore.QDateTime.currentDateTime().toMSecsSinceEpoch()
        key = uuid.uuid4().hex
        duration = 0

        if self._start_time_check.isChecked():
            start = self._start_time_edit.dateTime().toMSecsSinceEpoch()

        if self._duration_check.isChecked():
            duration_time = self._duration_edit.time()
            duration_secs = duration_time.hour() * 3600 + duration_time.minute() * 60
            duration = duration_secs * 1000

        return {
            'desc': desc,
            'project': project,
            'start': start,
            'duration': duration,
            'key': key,
        }

    def _reminder(self, activity):
        """Return configured reminder or None"""

        if not self._is_valid() or not activity:
            return None

        reminder_mins = 0
        if self._reminder_check.isChecked():
            reminder_mins = self._reminder_combo.currentData()

        if reminder_mins == 0:
            return None

        text = "You've been working for %d minutes" % reminder_mins
        key = activity['key']
        now = QtCore.QDateTime.currentMSecsSinceEpoch()
        time = now + reminder_mins * 1000 * 60

        return {
            'text': text,
            'time': time,
            'activity_key': key,
        }

    # public methods

    @classmethod
    def run(cls):
        """Display dialog, wait to finish, return (activity, reminder) tuple"""

        cont = cls()
        cont._setup()

        # FIXME: find a better way to raise dialog
        QtCore.QTimer.singleShot(300, lambda: cont._dialog.raise_())

        ret = cont._dialog.exec_()
        if ret == QtWidgets.QDialog.Rejected:
            return None, None

        activity = cont._activity()
        reminder = cont._reminder(activity)

        return activity, reminder

class RecentActivitiesModel(QtCore.QAbstractTableModel):
    """Data model for the recent activity table"""

    COL_START = 0
    COL_PROJECT = 1
    COL_DESC = 2
    COL_DURATION = 3
    COL_COUNT = 4

    # initialization

    def __init__(self, *args, **kwargs):
        """Initialize model with empty data"""

        super(RecentActivitiesModel, self).__init__(*args, **kwargs)
        self._activities = []

    # private methods

    def _row_count(self, parent = QtCore.QModelIndex()):
        """Return number of rows"""

        return len(self._activities)

    def _column_count(self, parent = QtCore.QModelIndex()):
        """Return number of columns"""

        return self.COL_COUNT

    def _header_data(self, section, orientation, role = QtCore.Qt.DisplayRole):
        """Return header for a given column"""

        default = QtCore.QVariant()

        if role != QtCore.Qt.DisplayRole:
            return default

        if orientation != QtCore.Qt.Horizontal:
            return default

        colnames = {
            self.COL_START: 'Start',
            self.COL_PROJECT: 'Project',
            self.COL_DESC: 'Activity',
            self.COL_DURATION: 'Duration',
        }

        return colnames.get(section, default)

    def _cell_data(self, index, role = QtCore.Qt.DisplayRole):
        """Return value for a given index"""

        if role != QtCore.Qt.DisplayRole:
            return QtCore.QVariant()

        row = index.row()
        col = index.column()

        activity = self._activities[row]
        value = ''

        if col == self.COL_START:
            msecs = activity['start']
            dt = QtCore.QDateTime.fromMSecsSinceEpoch(msecs)
            value = dt.toString('yyyy-MM-dd hh:mm')

        elif col == self.COL_PROJECT:
            value = activity['project']

        elif col == self.COL_DESC:
            value = activity['desc']

        elif col == self.COL_DURATION:
            value = common.format_duration(activity['duration'])

        return value

    # public methods

    def status_data(self, selected_indexes):
        """Return status summary of the selected indexes"""

        rows = (i.row() for i in selected_indexes)
        activities = (self._activities[r] for r in rows)

        count = len(selected_indexes)
        duration = sum(a['duration'] for a in activities)

        duration_text = common.format_duration(duration)
        text = "selected: %d items, total time: %s" % (count, duration_text)

        return text

    def reload_data(self):
        """Update activities and emit dataChanged signal"""

        self.layoutAboutToBeChanged.emit()
        self._activities = copy.deepcopy(app().recent_activities())
        self.layoutChanged.emit()

    # QAbstractTableModel methods

    def rowCount(self, parent = QtCore.QModelIndex()):
        return self._row_count(parent)

    def columnCount(self, parent = QtCore.QModelIndex()):
        return self._column_count(parent)

    def headerData(self, section, orientation, role = QtCore.Qt.DisplayRole):
        return self._header_data(section, orientation, role)

    def data(self, index, role = QtCore.Qt.DisplayRole):
        return self._cell_data(index, role)

class RecentActivitiesController:
    """Recent-activity window controller"""

    # initialization

    def __init__(self):
        """Initialize"""

        self._window = None
        self._table_model = None
        self._table_view = None
        self._status_label = None

    # private methods

    def _update_status(self):
        """Update status bar, according to the current selection"""

        indexes = self._table_view.selectionModel().selectedRows()
        text = self._table_model.status_data(indexes)
        self._status_label.setText(text)

    def _setup(self):
        """Setup widgets"""

        # table model
        self._table_model = RecentActivitiesModel()
        self._table_model.reload_data()

        # table view
        self._table_view = QtWidgets.QTableView()
        self._table_view.setModel(self._table_model)
        self._table_view.setFrameStyle(QtWidgets.QFrame.NoFrame)
        self._table_view.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self._table_view.verticalHeader().hide()
        self._table_view.verticalHeader().setDefaultSectionSize(25)
        self._table_view.setAlternatingRowColors(True)
        self._table_view.selectionModel().selectionChanged.connect(lambda a, b: self._update_status())
        self._table_view.setStyleSheet("""
            QTableView::item { padding: 0 5px 0 5px; border: 0; }
            QTableView { selection-background-color: #d0d0d0; selection-color: black; }
        """);

        # table header
        header = self._table_view.horizontalHeader()
        header.setSectionResizeMode(RecentActivitiesModel.COL_START, QtWidgets.QHeaderView.ResizeToContents)
        header.setSectionResizeMode(RecentActivitiesModel.COL_PROJECT, QtWidgets.QHeaderView.ResizeToContents)
        header.setSectionResizeMode(RecentActivitiesModel.COL_DESC, QtWidgets.QHeaderView.Stretch)
        header.setSectionResizeMode(RecentActivitiesModel.COL_DURATION, QtWidgets.QHeaderView.ResizeToContents)

        # horizontal line
        hline = QtWidgets.QFrame()
        hline.setFrameStyle(QtWidgets.QFrame.HLine | QtWidgets.QFrame.Sunken)

        # status label
        self._status_label = QtWidgets.QLabel()
        self._status_label.setMinimumHeight(24)
        self._status_label.setAlignment(QtCore.Qt.AlignCenter)
        status_font = self._status_label.font()
        if sys.platform == 'darwin':
            status_font.setPointSize(status_font.pointSize() - 2)
        self._status_label.setFont(status_font)
        self._status_label.setStyleSheet("QLabel { margin-bottom: 2px; color: #535353; }")
        self._update_status()

        # main layout
        main_layout = QtWidgets.QVBoxLayout()
        main_layout.setSpacing(0)
        main_layout.setContentsMargins(0, 0, 0, 0)
        main_layout.addWidget(self._table_view, 1)
        main_layout.addWidget(hline, 0)
        main_layout.addWidget(self._status_label, 0)

        # window flags
        flags = QtCore.Qt.CustomizeWindowHint | QtCore.Qt.WindowTitleHint | \
                QtCore.Qt.WindowCloseButtonHint | QtCore.Qt.WindowStaysOnTopHint

        # window
        self._window = QtWidgets.QWidget()
        self._window.resize(480, 240)
        self._window.setContentsMargins(0, 0, 0, 0)
        self._window.setWindowTitle('Recent activities')
        self._window.setWindowFlags(flags)
        self._window.setLayout(main_layout)
        app().position_window(self._window)

    # public methods

    def reload_data(self):
        """Reload activities"""

        self._table_model.reload_data()

    def show(self):
        """Display window"""

        if not self._window:
            self._setup()

        self._window.show()
        self._window.raise_()
        self._window.activateWindow()

if __name__ == "__main__":
    sys.exit(app().run())
