#
# Copyright (c) 2015 Łukasz Strzygowski
# Distributed under the terms of GPL-2 License.
#

"""
Shared utilities and helpers
"""

class Singleton(type):
    """Singleton metaclass"""

    instance = None

    def __call__(cls, *args, **kwargs):
        if not cls.instance:
            cls.instance = super(Singleton, cls).__call__(*args, **kwargs)
        return cls.instance

def format_duration(msecs):
    """Format milliseconds as H:MM:SS"""

    x = msecs // 1000
    hours = (x // 3600) % 60
    minutes = (x // 60) % 60
    seconds = x % 60
    text = "%d:%02d:%02d" % (hours, minutes, seconds)

    return text
