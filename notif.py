#!/usr/bin/env python3
#
# Copyright (c) 2015 Łukasz Strzygowski
# Distributed under the terms of GPL-2 License.
#

"""
Unobtrusive GMail notifier
"""

import argparse
import base64
import email
import email.header
import imaplib
import logging
import os
import re
import socket
import sys

import yaml

from PyQt5 import QtCore, QtGui, QtWidgets

import common

class App(metaclass = common.Singleton):
    """Global application manager"""

    def __init__(self):
        """Basic initialization"""

        self._root = os.path.dirname(sys.argv[0])
        self._messages = {}
        self._accounts = {}
        self._config = {}
        self._ignored_ids = set()
        self._gui = None
        self._worker_thread = None
        self._worker = None

    def _setup_logger(self, debug_mode):
        """Configure stderr and file loggers"""

        # common settings
        fmt = '%(asctime)s %(levelname)-8s %(message)s'
        datefmt = '%m-%d %H:%M'
        level = logging.DEBUG if debug_mode else logging.INFO

        # file output
        logging.basicConfig(
            level = level,
            format = fmt,
            datefmt = datefmt,
            filename = os.path.join(self.root, 'tmp', 'notif.log'),
            filemode = 'a'
        )

        # stderr output
        formatter = logging.Formatter(fmt, datefmt = datefmt)
        handler = logging.StreamHandler()
        handler.setLevel(level)
        handler.setFormatter(formatter)
        logger = logging.getLogger('')
        logger.addHandler(handler)

    def run(self):
        """Execute application"""

        # parse command line args
        parser = argparse.ArgumentParser(description='Unobtrusive GMail notifier')
        parser.add_argument('-d', '--debug', help='enable verbose logging', action='store_true')
        args = parser.parse_args()

        # setup logging
        self._setup_logger(args.debug)

        # load configuration file
        fp = open(os.path.join(self.root, 'config', 'notif.yml'))
        config = yaml.load(fp)
        for key, val in config.items():
            if key == 'config':
                continue
            self._accounts[key] = val
            self._accounts[key]['name'] = key
        self._config = config['config']

        # set default timeout for connections
        socket.setdefaulttimeout(self._config.get('timeout', 20))

        # create Qt application
        qapp = QtWidgets.QApplication(sys.argv)

        # setup user interface
        self._gui = GUIManager()
        self._gui.show()

        # setup background worker
        interval = self._config.get('interval', 60) * 1000
        self._worker_thread = QtCore.QThread()
        self._worker = Worker(interval)
        self._worker.account_updated.connect(self.account_updated)
        self._worker.status_changed.connect(self.status_changed)
        self._worker.moveToThread(self._worker_thread)
        self._worker_thread.started.connect(self._worker.run_timer)
        self._worker_thread.start()

        # start event loop
        ret = qapp.exec_()
        sys.exit(ret)

    @property
    def root(self):
        """Return application root directory"""

        return self._root

    @property
    def accounts(self):
        """Return dict of all accounts"""

        return self._accounts

    @property
    def messages(self):
        """Return dict of all messages"""

        return self._messages

    @property
    def visible_messages(self):
        """Return dict of all messages that are not ignored"""

        flt = lambda m: m['message_id'] not in self._ignored_ids
        msgs = {k: v for k, v in self.messages.items() if flt(v)}

        return msgs

    def visible_messages_for(self, account_name):
        """Return dict of visible messages for given account"""

        flt = lambda m: m['account_name'] == account_name
        msgs = {k: v for k, v in self.visible_messages.items() if flt(v)}

        return msgs

    def account_updated(self, account_name, messages):
        """Handle 'account updated' event from worker"""

        # remove previous messages for this account
        flt = lambda m: m['account_name'] != account_name
        new_messages = {k: v for k, v in self.messages.items() if flt(v)}

        # add current messages for this account
        new_messages.update(messages)
        self._messages = new_messages

        self._gui.update_account(account_name)

    def status_changed(self, status):
        """Handle 'status changed' event from worker"""

        self._gui.update_status(status)

    def check_now_clicked(self):
        """Handle 'Fetch now' action from GUI"""

        # send a signal to the background worker to start fetching immediately
        QtCore.QMetaObject.invokeMethod(self._worker, 'fetch_now', QtCore.Qt.QueuedConnection)

    def show_inbox_clicked(self, account_name):
        """Handle 'Show inbox in browser' action from GUI"""

        client = ClientFactory.client_for_account(account_name)
        url = client.inbox_url()
        QtGui.QDesktopServices.openUrl(QtCore.QUrl(url))

    def show_message_clicked(self, message_id):
        """Handle 'Show message in browser' action from GUI"""

        msg = self.messages[message_id]
        account_name = msg['account_name']
        client = ClientFactory.client_for_account(account_name)
        url = client.message_url(msg)
        QtGui.QDesktopServices.openUrl(QtCore.QUrl(url))

    def ignore_message_clicked(self, message_id):
        """Handle 'Mark as read' action from GUI"""

        message = self.messages[message_id]
        account_name = message['account_name']

        # mark message as ignored and hide it immediately
        self._ignored_ids.add(message_id)
        self._gui.update_account(account_name)

        # send a signal to the worker to mark it as read on the server
        QtCore.QMetaObject.invokeMethod(self._worker, 'mark_read', \
            QtCore.Qt.QueuedConnection, QtCore.Q_ARG(str, message_id))

class GUIManager:
    """Manager of the tray icon and menu"""

    def __init__(self):
        """Basic initialization, without invoking GUI"""

        self._menu = None
        self._status_action = None

        self._tray_icon = None
        self._active_icon = None
        self._inactive_icon = None

        self._account_submenus = {}
        self._message_submenus = {}

    def _setup_menu(self):
        """Setup menu for the tray icon"""

        app = App()
        self._menu = menu = QtWidgets.QMenu()

        # top section
        menu.addAction('Check now...', app.check_now_clicked)
        menu.addSeparator()

        # status section
        self._status_action = menu.addAction('Status: Ready')
        self._status_action.setEnabled(False)
        menu.addSeparator()

        # per-account-submenus section
        for name in sorted(app.accounts.keys()):
            cb = lambda name = name: app.show_inbox_clicked(name)
            submenu = menu.addMenu(name)
            submenu.addAction('Show in browser', cb)
            submenu.addSeparator()
            self._account_submenus[name] = submenu
            self._message_submenus[name] = {}
        menu.addSeparator()

        # bottom section
        menu.addAction('Exit', sys.exit)

    def _setup_icon(self):
        """Setup tray icon"""

        app = App()

        # load active/inactive icons
        icopath = os.path.join(app.root, 'icons')
        self._active_icon = QtGui.QIcon(os.path.join(icopath, 'notif-tray-active'))
        self._inactive_icon = QtGui.QIcon(os.path.join(icopath, 'notif-tray-inactive'))

        # create system tray icon widget
        self._tray_icon = QtWidgets.QSystemTrayIcon()
        self._tray_icon.setIcon(self._inactive_icon)
        self._tray_icon.setContextMenu(self._menu)
        self._tray_icon.activated.connect(self._icon_activated)

    def _icon_activated(self, reason):
        """Handle tray icon activation"""

        # display menu on a single left click, except for OSX, which handles it itself
        if reason == QtWidgets.QSystemTrayIcon.Trigger and sys.platform != 'darwin':
            pos = QtGui.QCursor.pos()
            self._menu.popup(pos)

    def show(self):
        """Initialize and show the GUI"""

        self._setup_menu()
        self._setup_icon()
        self._tray_icon.show()

    def update_status(self, status):
        """Update status message"""

        text = "Status: %s" % status
        self._status_action.setText(text)

    def update_account(self, account_name):
        """Update messages for the given account"""

        app = App()
        account_submenu = self._account_submenus[account_name]
        message_submenus = self._message_submenus[account_name]

        # find ids of added and removed messages
        messages = app.visible_messages_for(account_name)
        current_ids = set(m['message_id'] for m in messages.values())
        prev_ids = set(message_submenus.keys())
        added_ids = current_ids - prev_ids
        deleted_ids = prev_ids - current_ids

        # add submenus for new messages
        for mid in added_ids:
            message = app.messages[mid]

            # build title
            maxlen = 50
            title = "%s: %s" % (message['sender_short'], message['subject'])
            title = (title[0:maxlen] + '...') if len(title) >= maxlen else title

            # build actions
            show_cb = lambda mid = mid: app.show_message_clicked(mid)
            ignore_cb = lambda mid = mid: app.ignore_message_clicked(mid)

            # create submenu
            submenu = account_submenu.addMenu(title)
            submenu.addAction('Show in browser', show_cb)
            submenu.addAction('Mark as read', ignore_cb)
            message_submenus[mid] = submenu

        # remove submenus for deleted messages
        for mid in deleted_ids:
            submenu = message_submenus[mid]
            action = submenu.menuAction()
            account_submenu.removeAction(action)
            message_submenus.pop(mid)

        # update account submenu title
        count = len(current_ids)
        title = "%s (%d new)" % (account_name, count) if count > 0 else account_name
        account_submenu.setTitle(title)

        # update tray icon
        active = len(app.visible_messages) > 0
        icon = self._active_icon if active else self._inactive_icon
        self._tray_icon.setIcon(icon)

class ClientFactory:
    """Factory for email provider clients"""

    @classmethod
    def client_for_account(cls, account_name):
        """Return client for an account with given name"""

        app = App()
        account = app.accounts[account_name]
        provider = account['provider']
        classes = { 'gmail': GMailClient }
        client = classes[provider](account)

        return client

class GMailClient:
    """IMAP client for Google Mail"""

    def __init__(self, account):
        """Basic initialization, without connecting"""

        self._login = account['login']
        self._password = base64.b64decode(account['password']).decode()
        self._account_name = account['name']

    def _connect(self, readonly = False):
        """Open new connection"""

        conn = imaplib.IMAP4_SSL('imap.gmail.com')
        conn.login(self._login, self._password)
        conn.select(readonly = readonly)

        return conn

    def _parse_header(self, raw_header):
        """Parse raw header to a string"""

        decoded = email.header.decode_header(raw_header)
        header = email.header.make_header(decoded)

        return str(header)

    def _parse_message(self, uid, data):
        """Parse message from raw data"""

        body = data[0][1].decode('utf-8')
        msg = email.message_from_string(body)

        # extract gmail thread-id
        hdr = data[0][0].decode('utf-8')
        match = re.search(r'X-GM-THRID (\d+)', hdr)
        thread_id = int(match.groups(1)[0]) if match else ''

        # extract subject and sender
        subject = self._parse_header(msg['subject'])
        sender = self._parse_header(msg['from'])
        sender_name, sender_email = email.utils.parseaddr(sender)
        sender_short = sender_name if sender_name else sender_email

        return {
            'message_id': "%s:%s" % (self._account_name, uid),
            'uid': uid,
            'account_name': self._account_name,
            'subject': subject,
            'sender': sender,
            'sender_short': sender_short,
            'gmail_thread_id': thread_id,
        }

    def message_url(self, message):
        """Return URL for a given message"""

        login = self._login
        thread_id = message['gmail_thread_id']

        return "https://mail.google.com/mail/u/%s@gmail.com/#inbox/%x" % (login, thread_id)

    def inbox_url(self):
        """Return URL for the inbox"""

        return "https://mail.google.com/mail/u/%s@gmail.com/" % self._login

    def fetch_unseen(self):
        """Fetch unseen messages"""

        messages = {}
        conn = self._connect(readonly = True)

        # get list of UIDs of unseen messages
        ok, uids = conn.search(None, '(UNSEEN)')
        assert ok == 'OK'
        uids = uids[0].decode()
        uids = uids.split(' ') if uids else []

        # fetch all unseen messages
        for uid in uids:
            ok, data = conn.fetch(uid, '(BODY.PEEK[HEADER] FLAGS X-GM-THRID)')
            assert ok == 'OK'

            message = self._parse_message(uid, data)
            message_id = message['message_id']
            messages[message_id] = message

        return messages

    def mark_read(self, uid):
        """Mark message with given uid as read"""

        conn = self._connect()
        conn.store(uid, '+FLAGS', '\\SEEN')

class Worker(QtCore.QObject):
    """Background thread worker"""

    account_updated = QtCore.pyqtSignal(str, dict)
    status_changed = QtCore.pyqtSignal(str)

    def __init__(self, interval, *args, **kwargs):
        """Basic initialization"""

        super(Worker, self).__init__(*args, **kwargs)
        self._interval = interval
        self._timer = None

    def run_timer(self):
        """Execute periodic timer"""

        self._timer = QtCore.QTimer()
        self._timer.setInterval(self._interval)
        self._timer.timeout.connect(self._fetch_all)
        self._timer.start()
        self._fetch_all()

    @QtCore.pyqtSlot()
    def fetch_now(self):
        """Fetch messages immediately"""

        self._fetch_all()

    @QtCore.pyqtSlot(str)
    def mark_read(self, message_id):
        """Mark given message as read"""

        self._mark_read(message_id)

    def _fetch_all(self):
        """Fetch messages from all accounts. Emit status_changed"""

        app = App()

        # fetch from all accounts and set appropriate statuses
        ok = True
        self.status_changed.emit('Checking...')
        for account_name in app.accounts.keys():
            ok = ok and self._fetch_one(account_name)
        self.status_changed.emit('Ready' if ok else 'Error')

    def _fetch_one(self, account_name):
        """Fetch messages from a given account. Emit account_updated"""

        client = ClientFactory.client_for_account(account_name)

        # fetch messages
        logging.debug("Fetching from %s", account_name)
        try:
            messages = client.fetch_unseen()
        except Exception as e:
            logging.exception(e)
            return False
        logging.debug("Done fetching from %s", account_name)

        # notify app manager
        self.account_updated.emit(account_name, messages)

        return True

    def _mark_read(self, message_id):
        """Mark given message as read"""

        app = App()

        # load message, in case it was already removed, fail silently
        message = app.messages.get(message_id)
        if not message:
            return

        # load client for the related account
        account_name = message['account_name']
        client = ClientFactory.client_for_account(account_name)

        # try to mark message as read, log errors but fail silently
        try:
            logging.debug("Marking %s as read", message_id)
            client.mark_read(message['uid'])
            logging.debug("Done marking %s as read", message_id)
        except Exception as e:
            logging.exception(e)

if __name__ == "__main__":
    App().run()
